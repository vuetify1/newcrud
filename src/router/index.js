import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/crud',
    name: 'CRUD',
    meta: {
      requiresAuth: true
    },
    component: () => import('../components/CRUD.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
